// const jwt = require('jsonwebtoken');

// const auth = (req, res, next) => {
//     try {
//         const token = req.header('Authorization');
//         if (!token) return res.status(400).json({ msg: 'INVALID AUTHENTICATION!!!' });

//         jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
//             req.user = user;
//             next();
//         });
//     } catch (err) {
//         return res.status(500).json({ msg: err.message });
//     }
// }

// module.exports = auth;

const Users = require('../models/userModel');

const authAdmin = async (req, res, next) => {
    try {

        const user = await Users.findOne({
            _id: req.user.id
        });

        if (user.role === 0) return res.status(400).json({ msg: 'ACCESS DENIED!!!' });

        next();

    } catch (err) {
        return res.status(500).json({ msg: err.message });
    }
}

module.exports = authAdmin;