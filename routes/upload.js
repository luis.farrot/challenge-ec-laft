const auth = require('../middlewares/auth');
const authAdmin = require('../middlewares/authAdmin');
const cloudinary = require('cloudinary');
const router = require('express').Router();
const fs = require('fs');

// -----------------------------------------------------------------------------------------------------

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
})

// DELETE IMAGE PRODUCT
router.post('/delete', auth, authAdmin, (req, res) => {

    try {
        const { public_id } = req.body;
        if (!public_id) return res.status(400).json({ msg: 'NO IMAGES SELECTED!!!' });

        cloudinary.v2.uploader.destroy(public_id, async (err, result) => {
            if (err) throw err;

            res.json({ msg: 'DELETED IMAGE!!!' })
        })

    } catch (err) {
        return res.status(500).json({ msg: err.message });
    }
})

// UPLOAD IMAGE PRODUCTS
router.post('/upload', auth, authAdmin, (req, res) => {
    try {
        console.log(req.files);

        if (!req.files || Object.keys(req.files).length === 0)
            return res.status(400).json({ msg: 'NO FILES WERE UPLOADED!!!' });

        const file = req.files.file;

        if (file.size > 1024 * 1024) {
            removeTempFiles(file.tempFilePath);
            return res.status(400).send({ msg: 'SIZE IS OVER 1MB!!!' });
        }

        if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png') {
            removeTempFiles(file.tempFilePath);
            return res.status(400).send({ msg: 'INVALID FORMAT!!!' });
        }

        cloudinary.v2.uploader.upload(file.tempFilePath, { folder: 'test' }, async (err, result) => {
            if (err) throw err;
            removeTempFiles(file.tempFilePath);
            res.json({
                public_id: result.public_id,
                url: result.secure_url
            });
        })


    } catch (err) {
        return res.status(500).json({ msg: err.message });
    }
})

// REMOVE TEMPORALY FILES
const removeTempFiles = (path) => {
    fs.unlink(path, err => {
        if (err) throw err;
    })
}

// -----------------------------------------------------------------------------------------------------

module.exports = router