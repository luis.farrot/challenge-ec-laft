const auth = require('../middlewares/auth');
const router = require('express').Router();
const userController = require('../controllers/userController');

// -----------------------------------------------------------------------------------------------------

router.get('/info', auth, userController.getUser);

router.post('/login', userController.login);

router.get('/logout', userController.logout);

router.post('/register', userController.register);

router.get('/refresh_token', userController.refreshToken);

// -----------------------------------------------------------------------------------------------------

module.exports = router;