const auth = require('../middlewares/auth');
const authAdmin = require('../middlewares/authAdmin');
const roleController = require('../controllers/roleController');
const router = require('express').Router();

// -----------------------------------------------------------------------------------------------------

router.route('/role')
    .get(roleController.getRoles)
    .post(auth, authAdmin, roleController.createRole);

router.route('/role/:id')
    .delete(auth, authAdmin, roleController.deleteRole)
    .put(auth, authAdmin, roleController.updateRole);

// -----------------------------------------------------------------------------------------------------

module.exports = router;