require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const fileUpload = require('express-fileUpload');
const cookieParser = require('cookie-parser');

const app = express();
app.use(express.json());
app.use(cookieParser());
app.use(cors());
app.use(fileUpload({
    useTempFiles: true
}));

// ROUTES
app.use('/api', require('./routes/productRouter'));
app.use('/api', require('./routes/roleRouter'));
app.use('/api', require('./routes/upload'));
app.use('/user', require('./routes/userRouter'));

// CONNECT TO MONGODB
const URI = process.env.MONGODB_URL;
mongoose.connect(URI, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}, err => {
    if (err) throw err;
    console.log('CONNECTED TO MONGODB!!')
});

app.get('/', (req, res) => {
    res.json({
        msg: "CONNECT!!"
    })
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
    console.log('SERVER IS RUNNING ON PORT ', PORT);
})