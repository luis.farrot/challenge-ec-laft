const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    product_id: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    product_name: {
        type: String,
        required: true,
        trim: true
    },
    product_description: {
        type: String,
        required: true
    },
    product_price: {
        type: String,
        required: true,
        trim: true
    },
    product_image: {
        type: Object,
        required: true
    },
    product_category: {
        type: String,
        required: true
    },
    product_checked: {
        type: Boolean,
        default: false
    },
    product_stock: {
        type: Number,
        default: 0
    },
}, {
    timestamps: true
})

module.exports = mongoose.model('Products', productSchema);