const Users = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const userController = {

    // GET USER INFO
    getUser: async (req, res) => {
        try {
            const user = await Users.findById(req.user.id)
                .select('-password');
            if (!user) return res.status(400).json({ msg: 'Cannot find this user!!!' });
            res.json(user);
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    // USER LOGIN
    login: async (req, res) => {
        try {
            const {
                email,
                password
            } = req.body;

            const user = await Users.findOne({ email });
            if (!user) return res.status(400).json({ msg: "Cannot find this user!!!" });

            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) return res.status(400).json({ msg: "Incorrect password!!!" });

            // JWT AUTH

            const accessToken = createAccessToken({
                id: user._id
            });

            const refreshToken = createRefreshToken({
                id: user._id
            });

            res.cookie('refreshToken', refreshToken, {
                httpOnly: true,
                path: '/user/refresh_token'
            });

            // LOGIN
            res.json({ accessToken });
            // res.json({ msg: 'Login Success!!!' });


        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    // USER LOGOUT
    logout: async (req, res) => {
        try {
            res.clearCookie('refreshToken', { path: '/user/refresh_token' });
            return res.json({ msg: 'LOG OUT!!!' })
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    // REGISTER NEW USER
    register: async (req, res) => {

        try {

            const {
                name,
                surname,
                email,
                address,
                password
            } = req.body;

            const user = await Users.findOne({ email });
            if (user) return res.status(400).json({ msg: 'The email already exist!!!' });

            if (password.length < 8)
                return res.status(400).json({ msg: 'The password should have more than 8 characters!!!' });

            // PASSWORD ENCRYPTION
            const passwordHash = await bcrypt.hash(password, 10);

            // REGISTER NEW USER
            const newUser = new Users({
                name,
                surname,
                email,
                address,
                password: passwordHash
            });

            // SAVE NEW USER
            await newUser.save();

            // JWT AUTH

            const accessToken = createAccessToken({
                id: newUser._id
            });

            const refreshToken = createRefreshToken({
                id: newUser._id
            });

            res.cookie('refreshToken', refreshToken, {
                httpOnly: true,
                path: '/user/refresh_token'
            });

            // res.json({ msg: 'Register Success!!!' });
            res.json(accessToken);

        } catch (err) {
            return res.this.status(500).json({ msg: err.message });
        }

    },

    // REFRESH ACCESS TOKEN
    refreshToken: (req, res) => {

        try {

            const rf_tkn = req.cookies.refreshToken;
            if (!rf_tkn) return res.status(400).json({ msg: 'Please Login or Register!!!' });

            jwt.verify(rf_tkn, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
                if (err) return res.status(400).json({ msg: 'Please Login or Register!!!' });
                const accessToken = createAccessToken({ id: user.id });
                res.json({ accessToken });
            });


            res.json({ rf_tkn });

        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }



    }
}

const createAccessToken = (user) => {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1d' });
}

const createRefreshToken = (user) => {
    return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, { expiresIn: '3d' });
}

module.exports = userController;