const Products = require('../models/productModel');

// 
class APIfeatures {
    constructor(query, queryString) {
        this.query = query;
        this.queryString = queryString;
    }

    filtering() {
        const queryObj = { ...this.queryString };
        const excludeFields = ['page', 'sort', 'limit'];
        excludeFields.forEach(el => delete (queryObj[el]));

        let qStr = JSON.stringify(queryObj);
        qStr = qStr.replace(/\b(gte|gt|lt|lte|regex)\b/g, match => '$' + match);

        this.query.find(JSON.parse(qStr));

        return this;
    }

    sorting() {
        if (this.queryString.sort) {
            const sortBy = this.queryString.sort.split(',').join('  ');
            this.query = this.query.sort(sortBy);
        } else {
            this.query = this.query.sort('-createdAt');
        }

        return this;
    }

    paginating() {

        const page = this.queryString.page * 1 || 1;
        const limit = this.queryString.limit * 1 || 5;
        const skip = (page - 1) * limit;

        this.query = this.query.skip(skip).limit(limit);


        return this;
    }


}

//
const productController = {

    // CREATE NEW PRODUCT
    createProduct: async (req, res) => {
        try {
            const {
                product_id,
                product_name,
                product_price,
                product_description,
                product_image,
                product_category
            } = req.body;

            if (!product_image) return res.status(400).json({ msg: 'NO IMAGE UPLOAD!!!' });

            const product = await Products.findOne({ product_id });
            if (product) return res.status(400).json({ msg: 'THIS PRODUCT ALREADY EXISTS!!!' });

            // CREATING NEW PRODUCT
            const newProduct = new Products({
                product_id,
                product_name: product_name.toLowerCase(),
                product_price,
                product_description,
                product_image,
                product_category
            });

            await newProduct.save();
            res.json({ msg: "CREATED NEW PRODUCT IN INVENTARY!!!" });


        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    // DELETE NEW PRODUCT
    deleteProduct: async (req, res) => {
        try {
            await Products.findByIdAndDelete(req.params.id);
            res.json({ msg: 'DELETED PRODUCT!!!' });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    // GET PRODUCTS INFO
    getProducts: async (req, res) => {
        try {
            const features = new APIfeatures(Products.find(), req.query)
                .filtering()
                .sorting()
                .paginating();

            const products = await features.query;

            // const products = await Products.find();

            // res.json(products);

            res.json({
                status: 'success',
                result: products.length,
                products: products
            });
        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    // UPDATE NEW PRODUCT
    updateProduct: async (req, res) => {
        try {
            const {
                product_id,
                product_name,
                product_price,
                product_description,
                product_image,
                product_category
            } = req.body;

            if (!product_image) return res.status(400).json({ msg: 'NO IMAGE UPLOAD!!!' });

            // UPDATING PRODUCT
            await Products.findOneAndUpdate({ _id: req.params.id }, {
                product_name: product_name.toLowerCase(),
                product_price,
                product_description,
                product_image,
                product_category
            });

            res.json({ msg: "UPDATED PRODUCT IN INVENTARY!!!" });

        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    }


}


module.exports = productController;


