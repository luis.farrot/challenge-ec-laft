const Role = require('../models/roleModel');

const roleController = {

    // GET ROLE INFO
    getRoles: async (req, res) => {
        try {

            const roles = await Role.find();
            res.json(roles);

        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    // CREATE ROLE
    createRole: async (req, res) => {
        try {

            const { name } = req.body;
            const role = await Role.findOne({ name });

            if (role) return res.status(400).json({ msg: 'THIS ROLE IS ALREADY ACTIVE!!!' });


            // CREATING ROLE
            const newRole = new Role({ name });
            await newRole.save();

            res.json('Role Success!!!');

        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    // DELETE ROLE
    deleteRole: async (req, res) => {
        try {

            await Role.findByIdAndDelete(req.params.id);
            res.json({ msg: 'ROLE DELETED!!!' });


        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

    // UPDATE ROLE
    updateRole: async (req, res) => {
        try {

            const { name } = req.body;
            await Role.findOneAndUpdate(
                { _id: req.params.id },
                { name });
            res.json({ msg: 'ROLE UPDATED!!!' });


        } catch (err) {
            return res.status(500).json({ msg: err.message });
        }
    },

}

module.exports = roleController;